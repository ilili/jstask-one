<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
<%@include file="/WEB-INF/components/head.jsp" %>
<body>
    <%@include file="/WEB-INF/components/header.jsp" %>
    <div class="available-field">
        <div class="content-container">
            <div class="page-header">Авторизация</div>
                <form method="post" action="login.jsp" novalidate="" id="signin-form">
                    <div class="auth-box">
                        <label id="login_label" for="input_login">Login:</label><input id="input_login" class="input" type="text" name="username" autocapitalize="none" autocomplete="username" required="" minlength="3" maxlength="60" placeholder="Логин" tabindex="0">
                    
                        <div class="auth-field">
                            <div class="input-box">
                                <label id="label_password" for="input_password">Password:</label><input id="input_password" class="input" id="input_password" type="password" name="password" autocomplete="password" required="" minlength="3" maxlength="160" placeholder="Пароль" tabindex="0">
                            </div>
                        </div>
                
                        <div class="auth-field">
                            <button type="submit" title="Войти">Войти</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
<%@include file="/WEB-INF/components/footer.jsp" %>
</body>
</html>