<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<body>
<jsp:include page="../fragments/header.jsp"/>
<div class="container">

    <spring:url value="/debug" var="debugActionUrl" />
    <form:form class="form-horizontal" id="debug-form" method="post" action="${debugActionUrl}">

<%--        <spring:bind path="name">--%>
<%--            <div class="form-group ${status.error ? 'has-error' : ''}">--%>
<%--                <label class="col-sm-2 control-label">Name</label>--%>
<%--                <div class="col-sm-10">--%>
<%--                    <form:input path="name" type="text" class="form-control" id="name" placeholder="Name" />--%>
<%--                    <form:errors path="name" class="control-label" />--%>
<%--                </div>--%>
<%--            </div>--%>
<%--        </spring:bind>--%>

        <input class="input" type="text" name="error-number" autocapitalize="none" required="" minlength="3" maxlength="60" placeholder="Код ошибки" tabindex="0">
        <input class="input" type="text" name="error-description" autocapitalize="none" required="" minlength="3" maxlength="60" placeholder="Описание ошибки" tabindex="0">
        <button type="submit" title="Сломать">Сломать</button>
        <button type="submit" id="button-send" class="btn-sm btn-primary pull-right">Сломать</button>

    </form:form>
    <hr>
    <H3>${amount}</H3>
</div>
<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>