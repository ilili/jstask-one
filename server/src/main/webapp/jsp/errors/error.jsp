<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<body>
<jsp:include page="../fragments/header.jsp"/>
<div class="container">
    <h1>Something went wrong :(</h1>

    <div class="container">
        <h3>${exception.message}</h3>
        <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <a data-toggle="collapse" href="#collapse1">Details</a>
                    </h5>
                </div>
                <div id="collapse1" class="panel-collapse collapse">
                    ${exception.message}
                    <hr>
                    <c:forEach items="${exception.stackTrace}" var="stackTrace">
                        ${stackTrace}
                    </c:forEach>
                    <hr>
                        ${exception.toString()}
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../fragments/footer.jsp"/>
</body>
</html>

