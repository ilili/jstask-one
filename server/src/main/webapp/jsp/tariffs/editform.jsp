<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<body>
<jsp:include page="../fragments/header.jsp" />

<div class="container">
		<c:choose>
		<c:when test="${editForm['new']}">
			<h1>Add Tariff</h1>
		</c:when>
		<c:otherwise>
			<h1>Update Tariff</h1>
		</c:otherwise>
	</c:choose>

	<spring:url value="/users/list" var="userActionUrl" />

	<form:form class="form-horizontal" method="post" modelAttribute="editForm" action="${userActionUrl}">

		<spring:bind path="login">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label for="login" class="col-sm-2 control-label">Login</label>
				<div class="col-sm-10">
					<form:input path="login" type="text" class="form-control" id="login" placeholder="Login" />
					<form:errors path="login" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="firstName">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label for="firstName" class="col-sm-2 control-label">First name</label>
				<div class="col-sm-10">
					<form:input path="firstName" type="text" class="form-control" id="firstName" placeholder="First name" />
					<form:errors path="firstName" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="lastName">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label for="lastName" class="col-sm-2 control-label">Last name</label>
				<div class="col-sm-10">
					<form:input path="lastName" type="text" class="form-control" id="lastName" placeholder="Last name" />
					<form:errors path="lastName" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="email">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label for="email" class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10">
					<form:input path="email" class="form-control" id="email" placeholder="Email" />
					<form:errors path="email" class="control-label" />
				</div>
			</div>
		</spring:bind>

        <spring:bind path="birthDate">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label for="email" class="col-sm-2 control-label">Birth date</label>
                <div class="col-sm-10">
                    <form:input path="email" class="form-control" id="birthDate" placeholder="dd/mm/yyyy" />
                    <form:errors path="email" class="control-label" />
                </div>
            </div>
        </spring:bind>

		<spring:bind path="userRole">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">User role</label>
				<div class="col-sm-5">
					<form:select path="userRole" class="form-control">
						<form:option value="NONE" label="--- Select ---" />
						<form:options items="${listOfRoles}" />
					</form:select>
					<form:errors path="userRole" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="passwordHash">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label for="password" class="col-sm-2 control-label">Password</label>
				<div class="col-sm-10">
					<form:password path="passwordHash" class="form-control" id="password" placeholder="Password" />
					<form:errors path="passwordHash" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="address">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">Address</label>
				<div class="col-sm-10">
					<form:textarea path="address" rows="5" class="form-control" id="address" placeholder="address" />
					<form:errors path="address" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<spring:bind path="passport">
			<div class="form-group ${status.error ? 'has-error' : ''}">
				<label class="col-sm-2 control-label">Passport</label>
				<div class="col-sm-10">
					<form:textarea path="passport" rows="5" class="form-control" id="address" placeholder="Passport or ID number" />
					<form:errors path="passport" class="control-label" />
				</div>
			</div>
		</spring:bind>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<c:choose>
					<c:when test="${userForm['new']}">
						<button type="submit" class="btn-lg btn-primary pull-right">Add</button>
					</c:when>
					<c:otherwise>
						<button type="submit" class="btn-lg btn-primary pull-right">Update</button>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</form:form>
</div>

<jsp:include page="../fragments/footer.jsp" />

</body>
</html>