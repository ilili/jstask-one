<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<body>

<jsp:include page="../fragments/header.jsp"/>

<div class="container">

    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>

    <h1>Tariff Detail</h1>

    <div class="row">
        <label id="label_id" for="tariff.id" class="col-sm-2">ID</label>
        <div id="tariff.id" class="col-sm-10">${tariff.id}</div>
    </div>

    <div class="row">
        <label id="label_name" for="user.name" class="col-sm-2">Name</label>
        <div id="user.name" class="col-sm-10">${tariff.name}</div>
    </div>

    <div class="row">
        <label id="label_price" for="tariff.price" class="col-sm-2">Price</label>
        <div id="tariff.price" class="col-sm-10">${tariff.price}</div>
    </div>

    <div class="row">
        <label id="label_createdBy" for="user.createdBy" class="col-sm-2">Created by</label>
        <div id="user.createdBy" class="col-sm-10">ID #${tariff.createdBy}, ${tariff.created}</div>
    </div>

    <div class="row">
        <label id="label_changedBy" for="user.changedBy" class="col-sm-2">Last change by</label>
        <div id="user.changedBy" class="col-sm-10">ID #${tariff.changedBy}, ${tariff.lastChange}</div>
    </div>

    <spring:url value="/contracts/${tariff.id}" var="manageContracts" />

    <div class="row">
        <label id="label_contracts" for="user.contracts.size" class="col-sm-2">Contracts</label>
        <div id="user.contracts.size" class="col-sm-2">${contractsAmount}
            <a id="link_contracts" href="${manageContracts}" class="col-sm-2">Manage</a></div>
    </div>
</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>