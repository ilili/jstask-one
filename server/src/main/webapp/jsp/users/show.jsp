<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page session="false" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="en">
<body>

<jsp:include page="../fragments/header.jsp"/>

<div class="container">

    <c:if test="${not empty msg}">
        <div class="alert alert-${css} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>${msg}</strong>
        </div>
    </c:if>

    <h1>User Detail</h1>

    <div class="row">
        <label id="label_id" for="user.id" class="col-sm-2">ID</label>
        <div id="user.id" class="col-sm-10">${user.id}</div>
    </div>

    <div class="row">
        <label id="label_login" for="user.login" class="col-sm-2">Login</label>
        <div id="user.login" class="col-sm-10">${user.login}</div>
    </div>

    <div class="row">
        <label id="label_name" for="name" class="col-sm-2">Name</label>
        <div id="name" class="col-sm-10">${user.firstName} ${user.lastName}</div>
    </div>

	<div class="row">
		<label id="label_email" for="user.email" class="col-sm-2">Email</label>
		<div id="user.email" class="col-sm-10">${user.email}</div>
	</div>

    <div class="row">
        <label id="label_birthDate" for="user.birthDate" class="col-sm-2">Birth date</label>
        <div id="user.birthDate" class="col-sm-10">${user.birthDate}</div>
    </div>

    <div class="row">
        <label id="label_userRole" for="user.userRole" class="col-sm-2">User role</label>
        <div id="user.userRole" class="col-sm-10">${user.userRole}</div>
    </div>

    <div class="row">
        <label id="label_address" for="user.address" class="col-sm-2">Address</label>
        <div id="user.address" class="col-sm-10">${user.address}</div>
    </div>

    <div class="row">
        <label id="label_passport" for="user.passport" class="col-sm-2">Passport or ID</label>
        <div id="user.passport" class="col-sm-10">${user.passport}</div>
    </div>

    <div class="row">
        <label id="label_createdBy" for="user.createdBy" class="col-sm-2">Created by</label>
        <div id="user.createdBy" class="col-sm-10">ID #${user.createdBy}, ${user.created}</div>
    </div>

    <div class="row">
        <label id="label_changedBy" for="user.changedBy" class="col-sm-2">Last change by</label>
        <div id="user.changedBy" class="col-sm-10">ID #${user.changedBy}, ${user.lastChange}</div>
    </div>

    <spring:url value="/contracts/${user.id}" var="manageContracts" />

    <div class="row">
		<label id="label_contracts" for="user.contracts.size" class="col-sm-2">Contracts</label>
		<div id="user.contracts.size" class="col-sm-2">${user.contracts.size()}
        <a id="link_contracts" href="${manageContracts}" class="col-sm-2">Manage</a></div>
	</div>
</div>

<jsp:include page="../fragments/footer.jsp"/>

</body>
</html>