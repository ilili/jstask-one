<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib prefix="jst" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
    <meta charset="utf-8">
    <title>Self-service</title>
    <link rel="stylesheet" href="<jst:url value="/resources/css/styles.css"/>">
</head>