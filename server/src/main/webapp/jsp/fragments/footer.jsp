<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<hr>
<div class="container text-muted font-weight-light">
	<label id="version" for="version_value">Version:</label>
	<span id="version_value">1.0.1-SNAPSHOT</span>
</div>

<spring:url value="/resources/js/hello.js" var="coreJs" />
<spring:url value="/resources/js/bootstrap.js" var="bootstrapJs" />
<spring:url value="/resources/js/jquery.min.js" var="jquery" />
<spring:url value="/resources/js/tools.js" var="tools" />

<script src="${jquery}"></script>
<script src="${coreJs}"></script>
<script src="${bootstrapJs}"></script>
<script src="${tools}"></script>