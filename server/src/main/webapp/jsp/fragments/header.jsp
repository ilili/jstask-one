<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<head>
	<title>Operator's self-service</title>

	<spring:url value="/resources/css/hello.css" var="coreCss" />
	<spring:url value="/resources/css/bootstrap.css" var="bootstrapCss" />
	<spring:url value="/resources/css/styles.css" var="mystyles" />


	<link href="${bootstrapCss}" rel="stylesheet" />
	<link href="${coreCss}" rel="stylesheet" />
</head>

<spring:url value="/" var="urlHome" />
<spring:url value="/users/add" var="urlAddUser" />
<spring:url value="/resources/img/home.png" var="homePic" />

<nav class="navbar navbar-expand-md navbar-light bg-light">
    <a class="navbar-brand" href="${urlHome}">Self-service</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-collapse w-100 order-1 order-md-0 dual-collapse2">

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" id="home" href="${urlHome}" title="Go home">
                    <img alt="Home" height="30px" src="${homePic}">
                </a>
            </li>

            <li class="nav-item dropdown" id="nav_users">
                <a class="nav-link dropdown-toggle" href="javascript:void(0);" id="navbar_users_dropdown"
                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Users
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <spring:url value="/users/add" var="users_add" />
                    <spring:url value="/users/list" var="users_list" />
                    <spring:url value="/users/search" var="users_search" />

                    <a class="dropdown-item" id="add_user" href="${users_add}">Add new user</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" id="list_users" href="${users_list}">List of all users</a>
                    <a class="dropdown-item" id="find_user" href="${users_search}">Find a user</a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="javascript:void(0);" id="navbar_phones_dropdown"
                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Phone numbers
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <spring:url value="/phones/search" var="phones_search" />

                    <a class="dropdown-item" id="find_contract_by_phone" href="${phones_search}">Find a contract by phone</a>
                </div>
            </li>

            <li class="nav-item dropdown" id="nav_tariffs">
                <a class="nav-link dropdown-toggle" href="javascript:void(0);" id="navbar_tariffs_dropdown"
                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Tariffs
                </a>
                <div class="dropdown-menu" aria-labelledby="navbar_tariff_dropdown">
                    <spring:url value="/tariffs/add" var="tariffs_add" />
                    <spring:url value="/tariffs/list" var="tariffs_list" />
                    <spring:url value="/tariffs/search" var="tariffs_search" />
                    <a class="dropdown-item" id="add_tariff" href="${tariffs_add}">Add new tariff</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="${tariffs_list}">List of all existing tariffs</a>
                    <a class="dropdown-item" href="${tariffs_search}">Find a tariff</a>
                </div>
            </li>

            <li class="nav-item">
                <spring:url value="/debug" var="debug_link" />
                <a class="nav-link" id="topnavbtn_debug" href="${debug_link}" title="Debug">::&nbsp;Debug&nbsp;::</a>
            </li>

        </ul>
    </div>

    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <spring:url value="/users/{id}" var="user_card" />
                <a class="nav-link" id="user_card_link" href="${user_card}" title="Personal cabinet">username</a>
            </li>
        </ul>
        <spring:url value="/logout" var="logout" />
        <form class="form-inline" id="logout_button" action="${logout}" value="logout" name="logout" method="get">
            <button class="btn btn-sm align-middle btn-outline-secondary" type="button">Logout</button>
        </form>
    </div>
</nav>