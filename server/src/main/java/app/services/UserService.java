package app.services;

import app.dto.EntityIntIdDto;

import java.util.List;

public interface UserService {

	EntityIntIdDto findById(Integer id);
	
	List<EntityIntIdDto> findAll();

	void saveOrUpdate(EntityIntIdDto user);
	
	void delete(int id);
	
}