package app.services;

import app.dao.TariffDao;
import app.dto.BaseObjectDto;
import app.dto.TariffDto;
import app.persistence.Tariff;
import app.util.FieldsHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("TariffService")
public class TariffService {
// "/tariffs/list"

	private TariffDao dao;

	@Autowired
	public void setDao(TariffDao dao) { this.dao = dao; }


	private void copyProperty(Object src, Object dst, Field field) {
		FieldsHelper.setFieldValue(src, dst, field.getName());
	}

	private void copyProperties(Tariff tariff, BaseObjectDto userDto) {
		List<Field> fieldsList = FieldsHelper.getAllFields(new ArrayList<>(), tariff.getClass());

		fieldsList.forEach(field -> copyProperty(tariff, userDto, field));
	}

	private BaseObjectDto mapParametersToDto(Tariff tariff, Class clazz) {
		BaseObjectDto tariffDto = new TariffDto();
		copyProperties(tariff, tariffDto);
		return tariffDto;
	}

	private List<BaseObjectDto> mapParametersToDtoInList(List<Tariff> tariffs, Class clazz) {
		return tariffs.stream().map(tariff -> mapParametersToDto(tariff, clazz))
		            .collect(Collectors.toList());
	}

	@Transactional
	public Optional<Tariff> findById(Integer id) {
		Optional<Tariff> object = dao.getById(id);
		if (object.isEmpty()) return Optional.empty();
		else return Optional.of((Tariff) object.get());
	}

	public Optional<TariffDto> findTariffById(Integer id) {
		Optional<Tariff> tariffOptional = findById(id);

		if (tariffOptional.isPresent()) {
			TariffDto tariffBasicDto = (TariffDto) mapParametersToDto(tariffOptional.get(), TariffDto.class);
			return Optional.ofNullable(tariffBasicDto);
		}
		return Optional.empty();
	}

	public Integer getContractsAmount(Integer id) {
		Optional<Tariff> tariffOptional = findById(id);
//
//		if (tariffOptional.isPresent()) {
//			TariffDto tariffBasicDto = (TariffDto) mapParametersToDto(tariffOptional.get(), TariffDto.class);
//			return Optional.ofNullable(tariffBasicDto);
//		}
//		return Optional.empty();
		return 0;
	}

	@Transactional
	 public List<Tariff> findAll() {
		return dao.getAll(app.persistence.Tariff.class);
	}

	@Transactional
	public List<TariffDto> findAllTariffs() {
		List<Tariff> tariffs = findAll();
		List<BaseObjectDto> userTariffDtosList = mapParametersToDtoInList(tariffs, TariffDto.class);

		List<TariffDto> resultingList = new ArrayList<>();
		if (userTariffDtosList != null) {
			resultingList = userTariffDtosList.stream().filter(Objects::nonNull)
			                                .map(o -> (TariffDto) o)
			                                .collect(Collectors.toList());
		}

		return resultingList;
	}

}
