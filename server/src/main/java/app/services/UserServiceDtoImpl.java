package app.services;

import app.dao.UserDaoImpl;
import app.dto.UserFullDto;
import app.dto.UserSafeDto;
import app.persistence.User;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

@Service("UserServiceDto")
public class UserServiceDtoImpl implements BasicService<UserFullDto> {

	private UserDaoImpl dao;

	@Autowired
	public void setDao(UserDaoImpl dao) { this.dao = dao; }

	private UserSafeDto mapParametersToDto(User user, UserSafeDto usdto) throws InvocationTargetException,
	                                                                            IllegalAccessException {
		BeanUtils.copyProperties(usdto, user);
		return usdto;
	}

	public UserFullDto getUserByEmail(String email) {
		return null;
	}

	public Long getUsersAmount() {
		return dao.getTotalAmount(User.class);
	}

	public int getAccessLevelById(Integer id) {
		return 0;
	}

	@Override public Optional<UserFullDto> findById(Integer id) {
		return null;
	}

	@Override public List<UserFullDto> findAll() {
		List<UserFullDto> x = dao.getAll(User.class);

		return null;
	}

	public List<UserSafeDto> findAllNoPwd() {
		return null;
	}

	@Override public void saveOrUpdate(UserFullDto udto) {
//		UserDto userDto = (UserDto) udto;
	}

	public void saveOrUpdate(UserSafeDto udto) {
//		UserDto userDto = (UserDto) udto;
	}

	@Override public void delete(Integer id) {

	}
}
