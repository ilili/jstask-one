package app.services;

import java.util.List;
import java.util.Optional;

public interface BasicService<T> {

	Optional<T> findById(Integer id);

	List<T> findAll();

	void saveOrUpdate(T bdo);

	void delete(Integer id);
}