package app.services;

import app.dao.UserDaoImpl;
import app.dto.BaseObjectDto;
import app.dto.UserBasicDto;
import app.dto.UserFullDto;
import app.dto.UserSafeDto;
import app.persistence.User;
import app.util.FieldsHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("UserService")
public class UserServiceImpl implements BasicService<User> {

	private UserDaoImpl dao;

	@Autowired
	public void setDao(UserDaoImpl dao) { this.dao = dao; }


	private void copyProperty(Object src, Object dst, Field field) {
		FieldsHelper.setFieldValue(src, dst, field.getName());
	}

	private void copyProperties(User user, BaseObjectDto userDto) {
		List<Field> fieldsList = FieldsHelper.getAllFields(new ArrayList<>(), user.getClass());

		fieldsList.forEach(field -> copyProperty(user, userDto, field));
	}

	private BaseObjectDto mapParametersToDto(User user, Class clazz) {
		BaseObjectDto userDto = UserDtoFabric.getNewUser(clazz);
		copyProperties(user, userDto);
		return userDto;
	}

	private List<BaseObjectDto> mapParametersToDtoInList(List<User> users, Class clazz) {
		return users.stream().map(user -> mapParametersToDto(user, clazz))
		            .collect(Collectors.toList());
	}

	public UserFullDto getUserByEmail(String email) {
		return null;
	}

	public Long getUsersAmount() {
		return dao.getTotalAmount(app.persistence.User.class);
	}

	public int getAccessLevelById(Integer id) {
		return 0;
	}

	@Transactional
	@Override public Optional<User> findById(Integer id) {
		Optional<User> object = dao.getById(id);
		if (object.isEmpty()) return Optional.empty();
		else return Optional.of((User) object.get());
	}

		public Optional<UserBasicDto> findUserBasicById(Integer id) {
		Optional<User> userOptional = findById(id);

		if (userOptional.isPresent()) {
			UserBasicDto userBasicDto = (UserBasicDto) mapParametersToDto(userOptional.get(), UserBasicDto.class);
			return Optional.ofNullable(userBasicDto);
		}
		return Optional.empty();
	}


	public Optional<UserSafeDto> findUserSafeById(Integer id) {
		Optional<User> userOptional = findById(id);

		if (userOptional.isPresent()) {
			UserSafeDto userSafeDto = (UserSafeDto) mapParametersToDto(userOptional.get(), UserSafeDto.class);
			return Optional.ofNullable(userSafeDto);
		}
		return Optional.empty();
	}


	public Optional<UserFullDto> findUserFullById(Integer id) {
		Optional<User> userOptional = findById(id);

		if (userOptional.isPresent()) {
			UserFullDto userSafeDto = (UserFullDto) mapParametersToDto(userOptional.get(), UserFullDto.class);
			return Optional.ofNullable(userSafeDto);
		}
		return Optional.empty();
	}


	@Override public List<User> findAll() {
		return dao.getAll(app.persistence.User.class);
	}

	@Transactional
	public List<UserFullDto> findAllUsersFull() {
		List<User> users = findAll();
		List<BaseObjectDto> userFullDtosList = mapParametersToDtoInList(users, UserFullDto.class);

		List<UserFullDto> resultingList = new ArrayList<>();
		if (userFullDtosList != null) {
			resultingList = userFullDtosList.stream().filter(Objects::nonNull)
			                                .map(o -> (UserFullDto) o)
			                                .collect(Collectors.toList());
		}

		return resultingList;
	}

	@Transactional
	public List<UserSafeDto> findAllUsersSafe() {
		List<User> users = findAll();
		List<BaseObjectDto> userSafeDtosList = mapParametersToDtoInList(users, UserSafeDto.class);

		List<UserSafeDto> resultingList = new ArrayList<>();
		if (userSafeDtosList != null) {
			resultingList = userSafeDtosList.stream().filter(Objects::nonNull)
			                                .map(o -> (UserSafeDto) o)
			                                .collect(Collectors.toList());
		}

		return resultingList;
	}

	@Transactional
	public List<UserBasicDto> findAllUsersBasic() {
		List<User> users = findAll();
		List<BaseObjectDto> userBasicDtosList = mapParametersToDtoInList(users, UserBasicDto.class);

		List<UserBasicDto> resultingList = new ArrayList<>();
		if (userBasicDtosList != null) {
			resultingList = userBasicDtosList.stream().filter(Objects::nonNull)
			                                 .map(o -> (UserBasicDto) o)
			                                 .collect(Collectors.toList());
		}

		return resultingList;
	}

//	public List<Contract> getContractsById(Integer id) {
//
//	}

	@Override public void saveOrUpdate(User udto) {
//		UserDto userDto = (UserDto) udto;
	}

	public void saveOrUpdate(UserSafeDto udto) {

		if (findById(udto.getId()).isEmpty()) {
			dao.save(udto);
		} else {
			dao.update(udto);
		}
	}

	public void saveOrUpdate(UserFullDto udto) {

		if (findById(udto.getId()).isEmpty()) {
			dao.save(udto);
		} else {
			dao.update(udto);
		}
	}

	@Override public void delete(Integer id) {
		dao.delete(id);
	}
}
