package app.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;

import java.lang.reflect.Field;
import java.util.*;
import java.util.stream.Collectors;

public class FieldsHelper {

	@Nullable
	static private Map<String, Field> getFieldsWithSameNameFromBothObjects(Object src, Object dst, String fieldName) {
		if (src == null | dst == null | StringUtils.isEmpty(fieldName)) return null;

		List<Field> srcFieldsList = getAllFields(new ArrayList<>(), src.getClass());
		List<String> srcFieldsListNames = srcFieldsList.stream().map(Field::getName).collect(Collectors.toList());

//		Field[] dstObjFieldsArray = dst.getClass().getDeclaredFields();
		List<Field> dstFieldsList = getAllFields(new ArrayList<>(), dst.getClass());
		List<String> dstFieldsListNames = dstFieldsList.stream().map(Field::getName).collect(Collectors.toList());

		if (!srcFieldsListNames.contains(fieldName) |
			!dstFieldsListNames.contains(fieldName)) return null;

		Field srcField = srcFieldsList.stream().filter(field -> fieldName.equals(field.getName())).findFirst().orElse(null);
		Field dstField = dstFieldsList.stream().filter(field -> fieldName.equals(field.getName())).findFirst().orElse(null);

		if (srcField == null | dstField == null) return null;

		Map<String, Field> result = new HashMap<>();
		result.put("src", srcField);
		result.put("dst", dstField);
		return result;
	}

	static public boolean isFieldTypeTheSameInBothObjects(Object src, Object dst, String fieldName) {
		HashMap<String, Field> fields = (HashMap<String, Field>) getFieldsWithSameNameFromBothObjects(src, dst, fieldName);

		if (fields == null) return false;

		Class srcFieldClass = fields.get("src").getType();
		Class dstFieldClass = fields.get("dst").getType();

		return srcFieldClass.equals(dstFieldClass);
	}

	public static List<Field> getAllFields(List<Field> fields, Class<?> type) {
		fields.addAll(Arrays.asList(type.getDeclaredFields()));

		if (type.getSuperclass() != null) {
			getAllFields(fields, type.getSuperclass());
		}

		return fields;
	}


	static public void setFieldValue(Object src, Object dst, String fieldName) {
		if (isFieldTypeTheSameInBothObjects(src, dst, fieldName)) {
			HashMap<String, Field> fields = (HashMap<String, Field>) getFieldsWithSameNameFromBothObjects(src, dst, fieldName);

			Field srcField = fields.get("src");
			Field dstField = fields.get("dst");

			srcField.setAccessible(true);
			dstField.setAccessible(true);

			try {
				dstField.set(dst, srcField.get(src));
			}
			catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}


	static public void mapSingleValue(Object object, Map.Entry<String, Object> mapEntry) {
//		Field[] objFieldsArray = object.getClass().getDeclaredFields();
		List<Field> fieldsList = getAllFields(new ArrayList<>(), object.getClass());

		if (mapEntry != null) {
			fieldsList.stream()
			          .filter(field -> mapEntry.getKey().equals(field.getName())) // Class field's name equals to map key
			          .filter(field -> {                                          // Class field's type equals to map value
				          field.setAccessible(true);
				          boolean result = false;
				          Class fieldClass = field.getType();
				          result = fieldClass.equals(mapEntry.getValue().getClass());
				          return result;
			          })
			          .forEach(field -> {                                         // Set field value
				          try {
					          Field fieldToSet = object.getClass().getDeclaredField(field.getName());
					          fieldToSet.setAccessible(true);
					          fieldToSet.set(object, mapEntry.getValue());
				          }
				          catch (IllegalAccessException | NoSuchFieldException e) { e.printStackTrace(); }
			          });
		}
	}
}
