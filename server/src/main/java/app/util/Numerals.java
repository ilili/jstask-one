package app.util;

public class Numerals {

	static public Integer stringToInteger(String string) throws NumberFormatException, NullPointerException {
		return Integer.parseInt(string);
	}

	static public boolean checkIsStringInt(String string) {
		try {
			Integer d = stringToInteger(string);
		}
		catch (NumberFormatException | NullPointerException nfe) {
			return false;
		}
		return true;
	}
}
