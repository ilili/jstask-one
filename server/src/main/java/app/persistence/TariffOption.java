package app.persistence;

import javax.persistence.*;

@Entity
@Table(name = "Tariff_options") @Embeddable
public class TariffOption extends TableIntId {

	@ManyToOne
	@JoinColumn(name = "TARIFF_ID", referencedColumnName = "ID", nullable = false)
	private Tariff tariff;

	@Basic
	@Column(name = "NAME", length = 120, nullable = false)
	private String name;

	@Basic
	@Column(name = "MATCHER", length = 128, nullable = false, unique = true)
	private String matcher;

	// todo control the float part
	//@Digits(integer = 10, fraction = 2)// fixme add validation
	@Column(name = "PRICE", nullable = false, columnDefinition = "DOUBLE(10,2) DEFAULT 0.00")
	private Double price;

	// todo control the float part
	//@Digits(integer = 10, fraction = 2)// fixme add validation
	@Column(name = "ACTIVATION_PRICE", nullable = false, columnDefinition = "DOUBLE(10,2) DEFAULT 0.00")
	private Double activationPrice;
}