package app.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.sql.Timestamp;

@Getter @Setter
@MappedSuperclass
public abstract class BaseObject {

	@Basic
	@Column(name = "IS_ACTIVE", columnDefinition = "BOOLEAN DEFAULT FALSE")
	protected boolean isActive;

	@Basic @Column(name = "CREATED_BY")
	 Integer createdBy;

	@Basic @Column(name = "CREATED")
	 Timestamp created;

	@Basic @Column(name = "CHANGED_BY")
	protected Integer changedBy;

	@Basic @Column(name = "LAST_CHANGE")
	protected Timestamp lastChange;
}
