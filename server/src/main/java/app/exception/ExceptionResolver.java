package app.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;

@ControllerAdvice
public class ExceptionResolver extends ExceptionHandlerExceptionResolver {

	private final Logger logger = LoggerFactory.getLogger(ExceptionResolver.class);
//
//	@ExceptionHandler(RuntimeException.class)
//	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Some terrible exception occured")
//	public String catchRuntimeException(HttpServletRequest request, Exception exception){
//		logger.error("RuntimeException occured: URL = " + request.getRequestURL());
//		return "errors/error";
//	}
//
//	@ExceptionHandler({javax.servlet.ServletException.class,
//		org.springframework.beans.NotReadablePropertyException.class})
//	public String catchException(HttpServletRequest request, Exception exception){
//		logger.error("ServletException occured: URL = " + request.getRequestURL());
//		return "errors/error";
//	}
//
//	@ExceptionHandler(SQLException.class)
//	public String handleSQLException(HttpServletRequest request, Exception exception){
//		logger.info("SQLException Occured:: URL="+request.getRequestURL());
//		return "errors/error";
//	}

	@ExceptionHandler({IllegalStateException.class,
		ServletException.class, SQLException.class, ServletException.class,
		LinkageError.class})
	@RequestMapping
//	@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Some terrible exception occured")
	public String catchUserRuntimeException(HttpServletRequest request, Exception exception, Model model){
		logger.error("RuntimeException occured: URL = " + request.getRequestURL() +"/n" + exception.toString());

		model.addAttribute("exception", exception);
		return "errors/error";
	}
}
