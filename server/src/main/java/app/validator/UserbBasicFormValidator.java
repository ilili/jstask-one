package app.validator;

import app.dto.UserBasicDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

//http://docs.spring.io/spring/docs/current/spring-framework-reference/html/validation.html#validation-mvc-configuring
@Component
public class UserbBasicFormValidator implements Validator {

//	@Autowired
//	@Qualifier("emailValidator")
//	EmailValidator emailValidator;
//
//	@Autowired
//	UserServiceImpl userService;

	@Override
//	public boolean supports(Class<?> clazz) {
//		return UserFullDto.class.equals(clazz);
//	}
//	public boolean supports(Class<?> clazz) {return UserSafeDto.class.equals(clazz);}
	public boolean supports(Class<?> clazz) {return true;}


	@Override
	public void validate(Object target, Errors errors) {

		UserBasicDto user = (UserBasicDto) target;

		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "NotEmpty.userForm.login");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "email", "NotEmpty.userForm.email");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "userRole", "NotEmpty.userForm.userRole");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.userForm.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.userForm.lastName");

//		if(!emailValidator.valid(user.getEmail())){
//			errors.rejectValue("email", "Pattern.userForm.email");
//		}
	}
}