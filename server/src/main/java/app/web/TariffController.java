package app.web;

import app.dto.TariffDto;
import app.services.TariffService;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller("TariffController")
public class TariffController {
	private final Logger logger = LoggerFactory.getLogger(TariffController.class);

	@Autowired
	private TariffService tariffService;

	// list page
	@Transactional
	@RequestMapping(value = {"/tariffs/list", "/tariffs"}, method = RequestMethod.GET)
	public String listTariffs(HttpServletRequest req, Model model) {
		logger.debug("listTariffs()");

		model.addAttribute("tariffs", tariffService.findAllTariffs());

		return "tariffs/list";
	}


	// show user
	@Transactional
	@RequestMapping(value = "/tariffs/{id}", method = RequestMethod.GET)
	public String showTariff(@PathVariable("id") int id, Model model) {
		logger.debug("showTariff() id: {}", id);

		Optional<TariffDto> tariffDto = tariffService.findTariffById(id);
		Integer contractsAmount = tariffService.getContractsAmount(id);

		if (tariffDto.isEmpty()) {
			model.addAttribute("css", "danger");
			model.addAttribute("msg", "User not found");
		} else {
			model.addAttribute("tariff", tariffDto.get());
			model.addAttribute("contractsAmount", contractsAmount);
		}

		return "tariffs/show";
	}


	@ExceptionHandler(HibernateException.class)
	@RequestMapping
	public String catchTariffRuntimeException(HttpServletRequest request, Exception exception, Model model){
		logger.error("RuntimeException occured: URL = " + request.getRequestURL() +"/n" + exception.toString());

		model.addAttribute("exception", exception);
		return "errors/error";
	}
}