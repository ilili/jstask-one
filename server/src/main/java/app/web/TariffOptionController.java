package app.web;

import app.services.TariffOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller("TariffOptionController")
public class TariffOptionController implements CrudController {

	@Autowired
	private TariffOptionService tariffOptionService;

	@Override
	@RequestMapping(value = "/add_tariff_option", method = RequestMethod.GET)
	public String create(HttpServletRequest req, Model model) {
		return "/add_tariff_option";
	}


	@Override
	@RequestMapping(value = "/tariff_option_card", method = RequestMethod.GET)
	public String showCard(HttpServletRequest req, Model model) {
		return "/tariff_option_card";
	}


	@Override
	@RequestMapping(value = "/edit_tariff_option", method = RequestMethod.GET)
	public String edit(HttpServletRequest req, Model model) {
		return "/edit_tariff_option";
	}


	@Override
	@RequestMapping(value = "/tariff_options_list", method = RequestMethod.GET)
	public String list(HttpServletRequest req, Model model) {
		return "/tariff_option_list";
	}


	@Override
	@RequestMapping(value = "/remove_tariff_option", method = RequestMethod.GET)
	public String remove(HttpServletRequest req, Model model) {
		return null;
	}
}