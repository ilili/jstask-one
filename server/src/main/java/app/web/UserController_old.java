//package app.web;
//
//import app.services.UserService;
//import app.validator.UserFormValidator;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.EmptyResultDataAccessException;
//import org.springframework.web.bind.WebDataBinder;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.InitBinder;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
////import javax.validation.Valid;
//
////http://www.tikalk.com/redirectattributes-new-feature-spring-mvc-31/
////https://en.wikipedia.org/wiki/Post/Redirect/Get
////http://www.oschina.net/translate/spring-mvc-flash-attribute-example
////@Controller
//public class UserController_old {
//
//	private final Logger logger = LoggerFactory.getLogger(UserController_old.class);
//
//	@Autowired
//	UserFormValidator userFormValidator;
//
//	@InitBinder
//	protected void initBinder(WebDataBinder binder) {
//		binder.setValidator(userFormValidator);
//	}
//
//	private UserService userService;
//
//	@Autowired
//	public void setUserService(UserService userService) {
//		this.userService = userService;
//	}
////
////	@RequestMapping(value = "/", method = RequestMethod.GET)
////	public String index(Model model) {
////		logger.debug("index()");
////		return "redirect:/users";
////	}
////
////	// list page
////	@RequestMapping(value = "/users", method = RequestMethod.GET)
////	public String showAllUsers(Model model) {
////
////		logger.debug("showAllUsers()");
////		model.addAttribute("users", userService.findAll());
////		return "users/list";
////	}
////
////	// save or update user
////	@RequestMapping(value = "/users", method = RequestMethod.POST)
////	public String saveOrUpdateUser(@ModelAttribute("userForm") @Validated User user,
////			BindingResult result, Model model, final RedirectAttributes redirectAttributes) {
////
////		logger.debug("saveOrUpdateUser() : {}", user);
////
////		if (result.hasErrors()) {
//////			populateDefaultModel(model);
////			return "users/userform";
////		} else {
////
////			userService.saveOrUpdate(user);
////
////			redirectAttributes.addFlashAttribute("css", "success");
////			if(user.isNew()){
////				redirectAttributes.addFlashAttribute("msg", "User added successfully!");
////			}else{
////				redirectAttributes.addFlashAttribute("msg", "User updated successfully!");
////			}
////
////
////			// POST/REDIRECT/GET
////			return "redirect:/users/" + user.getId();
////
////			// POST/FORWARD/GET
////			// return "user/list";
////
////		}
////
////	}
////
////	// show add user form
////	@RequestMapping(value = "/users/add1", method = RequestMethod.GET)
////	public String showAddUserForm(Model model) {
////
////		logger.debug("showAddUserForm()");
////
////		User user = new User();
////
////		// set default value
////		user.setName("test123");
////		user.setEmail("test@gmail.com");
////		user.setAddress("abc 88");
////		//user.setPassword("123");
////		//user.setConfirmPassword("123");
////		user.setNewsletter(true);
////		user.setSex("M");
////		user.setFramework(new ArrayList<String>(Arrays.asList("Spring MVC", "GWT")));
////		user.setSkill(new ArrayList<String>(Arrays.asList("Spring", "Grails", "Groovy")));
////		user.setCountry("SG");
////		user.setNumber(2);
////
////		model.addAttribute("userForm", user);
////
////		populateDefaultModel(model);
////
////		return "users/userform";
////
////	}
//
//	// show update form
////	@RequestMapping(value = "/users/{id}/update", method = RequestMethod.GET)
////	public String showUpdateUserForm(@PathVariable("id") int id, Model model) {
////
////		logger.debug("showUpdateUserForm() : {}", id);
////
////		User user = userService.findById(id);
////		model.addAttribute("userForm", user);
////
//////		populateDefaultModel(model);
////
////		return "users/userform";
////
////	}
//
//	// delete user
////	@RequestMapping(value = "/users/{id}/delete", method = RequestMethod.POST)
////	public String deleteUser(@PathVariable("id") int id, final RedirectAttributes redirectAttributes) {
////
////		logger.debug("deleteUser() : {}", id);
////
////		userService.delete(id);
////
////		redirectAttributes.addFlashAttribute("css", "success");
////		redirectAttributes.addFlashAttribute("msg", "User is deleted!");
////
////		return "redirect:/users";
//
////	}
////
////	// show user
////	@RequestMapping(value = "/users1/{id}", method = RequestMethod.GET)
////	public String showUser(@PathVariable("id") int id, Model model) {
////
////		logger.debug("showUser() id: {}", id);
////
////		User user = userService.findById(id);
////		if (user == null) {
////			model.addAttribute("css", "danger");
////			model.addAttribute("msg", "User not found");
////		}
////		model.addAttribute("user", user);
////
////		return "users/show1";
////
////	}
////
////	private void populateDefaultModel(Model model) {
////
////		List<String> frameworksList = new ArrayList<String>();
////		frameworksList.add("Spring MVC");
////		frameworksList.add("Struts 2");
////		frameworksList.add("JSF 2");
////		frameworksList.add("GWT");
////		frameworksList.add("Play");
////		frameworksList.add("Apache Wicket");
////		model.addAttribute("frameworkList", frameworksList);
////
////		Map<String, String> skill = new LinkedHashMap<String, String>();
////		skill.put("Hibernate", "Hibernate");
////		skill.put("Spring", "Spring");
////		skill.put("Struts", "Struts");
////		skill.put("Groovy", "Groovy");
////		skill.put("Grails", "Grails");
////		model.addAttribute("javaSkillList", skill);
////
////		List<Integer> numbers = new ArrayList<Integer>();
////		numbers.add(1);
////		numbers.add(2);
////		numbers.add(3);
////		numbers.add(4);
////		numbers.add(5);
////		model.addAttribute("numberList", numbers);
////
////		Map<String, String> country = new LinkedHashMap<String, String>();
////		country.put("US", "United Stated");
////		country.put("CN", "China");
////		country.put("SG", "Singapore");
////		country.put("MY", "Malaysia");
////		model.addAttribute("countryList", country);
////
////	}
////	}
//	@ExceptionHandler(EmptyResultDataAccessException.class)
//	public ModelAndView handleEmptyData(HttpServletRequest req, Exception ex) {
//
//		logger.debug("handleEmptyData()");
//		logger.error("Request: {}, error ", req.getRequestURL(), ex);
//
//		ModelAndView model = new ModelAndView();
//		model.setViewName("user/show");
//		model.addObject("msg", "user not found");
//
//		return model;
//
//	}
//
//}