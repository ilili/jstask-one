package app.web;

import app.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

//import dto.UserDTO;
//import exceptions.DuplicateUserException;
//import exceptions.UserNotFoundException;
//import security.UserDetailsServiceDAO;
//import app.services.interfaces.UserService;


@Controller
public class LoginController {

	@Autowired
	private UserServiceImpl userService;

//	@Autowired
//	private UserDetailsService userDetailsService;

//	@Autowired
////	@Qualifier("dao-auth")
//	private AuthenticationManager authenticationManager;

	/**
	 * Method for dispatching requests to login page
	 *
	 * @param model app.model for page view
	 * @return page for view login
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(HttpServletRequest req, Model model) {
		if (req.getHeader("referer") == null)
			return "/login";
		if (req.getHeader("referer").equals("http://localhost:8090/login"))
			model.addAttribute("error", "The username or password incorrect");
		return "/login";
	}
//
//	@RequestMapping(value = "/zzz", method = RequestMethod.GET)
//	public String zzz(HttpServletRequest req, Model model) {
//		System.out.println("qwer");
//
//		return "debug";
//	}
//
//	@RequestMapping(value = "/debug", method = RequestMethod.GET)
//	public String yyy(HttpServletRequest req, Model model) {
//		System.out.println("qwer");
//
//		return "other/debug";
//	}
//
//
//	@RequestMapping(value = "/xxx", method = RequestMethod.GET)
//	public String xxx(HttpServletRequest req, Model model) {
//		System.out.println("qwer1");
//		return "other/debug";
//	}

	/**
	 * Method for dispatching requests to logout page
	 *
	 * @param model app.model for page view
	 * @param req   request to page
	 * @return page for view login
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logoutPage(Model model, HttpServletRequest req) throws ServletException {
		req.getSession().invalidate();
		req.logout();
		return "redirect:/index";
	}

//	/**
//	 * Method for dispatching requests to remember password page
//	 *
//	 * @return page for view login
//	 */
//	@RequestMapping(value = "/rememberPassword", method = RequestMethod.GET)
//	public String rememberMe() {
//		return "all/rememberpassword";
//	}
//
//
//	@RequestMapping(value = "/rememberPassword", method = RequestMethod.POST)
//	public String rememberMePost(Model app.model, HttpServletRequest req, @RequestParam(value = "email") String email) {
//		String regexpEmail="(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
//		if (!Pattern.matches(regexpEmail , email)){
//			app.model.addAttribute("error", "Email field is required in right format xxx@xx.xx");
//			return "all/rememberpassword";
//		}
//		try {
//			UserDto user = userService.getUserByeMail(email);
//			app.model.addAttribute("notification", "Your password sent on your e-mail:" + user.getEmail());
//		} catch (UserNotFoundException ex) {
//			app.model.addAttribute("error", "User doesn't exist. Try again!");
//		}
//		return "all/rememberpassword";
//	}

//	/**
//	 * Method for dispatching requests to register page
//	 *
//	 * @param app.model app.model for page view
//	 * @return page for view register
//	 */
//	@RequestMapping(value = "/register", method = RequestMethod.GET)
//	public String registerPage(Model app.model) {
//		return "all/register";
//	}

//	/**
//	 * Method for dispatching requests to register page
//	 *
//	 * @param app.model app.model for page view
//	 * @return page for view register
//	 */
//	@RequestMapping(value = "/register", method = RequestMethod.POST)
//	public String registerPagePost(Model app.model, HttpServletRequest req,
//	                               @RequestParam(value = "email") String email,
//	                               @RequestParam(value = "phone") String phone,
//	                               @RequestParam(value = "password") String password,
//	                               @RequestParam(value = "confirm-password") String confirmPassword) {
//
//		String error = null;
//		app.model.addAttribute("email", email);
//		app.model.addAttribute("phone", phone);
//
//		String regexpEmail="(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
//		if (!Pattern.matches(regexpEmail , email)){
//			error = "Email field is required in right format xxx@xx.xx";
//			app.model.addAttribute("error", error);
//			return "all/register";
//		}
//		if (!Pattern.matches("^79[0-9]{9}$" , phone)){
//			error = "Phone Number must be in right format 79XXXXXXXXX";
//			app.model.addAttribute("error", error);
//			return "all/register";
//		}
//		if (password.length() < 5){
//			error = "Password must be over 5 characters.";
//			app.model.addAttribute("error", error);
//			return "all/register";
//		}
//		if (password.equals(confirmPassword)) {
//			try{// FIXME: 31.10.2019 repair it
////				userService.createUser(email, phone, password);
//				req.getSession().setAttribute("currentUser", userService.getUserByEmail(email));
//				autoLogin(email, password);
//				return "index";
//			} catch (UserAlreadyExistsException e){
//				error = e.getLocalizedMessage();
//			}
//		} else {
//			error = "Password and confirmation are not equal";
//		}
//
//		app.model.addAttribute("error", error);
//		return "all/register";
//	}


	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String loginPagePost(Model model, HttpServletRequest req,
	                            @RequestParam(value = "email") String email,
	                            @RequestParam(value = "password") String password) {
		req.getSession().setAttribute("currentUser", userService.getUserByEmail(email));
		return "redirect:/index";
	}

	@RequestMapping(value = "/denied", method = RequestMethod.GET)
	public String deniedPage(Model model) {
		return "all/403";
	}

	@RequestMapping(value = "/login-denied", method = RequestMethod.GET)
	public String loginDeniedPage(HttpServletRequest req, Model model) {
		return "all/logindenied";
	}
//
//	@RequestMapping(value = "/z", method = RequestMethod.GET)
//	public String mainPage(HttpServletRequest req, Model model) {
//		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		UserFullDto currentUser = userService.getUserByEmail(user.getUsername());
//		req.getSession().setAttribute("currentUser", currentUser);
//		if (userService.getAccessLevelById(currentUser.getId()) == 1) {
//			return "redirect:user/previous-orders";
//		} else if (userService.getAccessLevelById(currentUser.getId()) == 2) {
//			return "redirect:admin/order-history";
//		} else return "/index.jsp";
//	}

//	private void autoLogin(String username, String password) {
//		UserDetails userDetails = userDetailsService.loadUserByUsername(username);
//		UsernamePasswordAuthenticationToken authenticationToken =
//			new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
//		authenticationManager.authenticate(authenticationToken);
//		if (authenticationToken.isAuthenticated()) {
//			SecurityContextHolder.getContext().setAuthentication(authenticationToken);
//		}
//	}

}
