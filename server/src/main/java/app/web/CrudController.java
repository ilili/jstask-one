package app.web;

import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

public interface CrudController {

	public String create(HttpServletRequest req, Model model);

	public String showCard(HttpServletRequest req, Model model);

	public String edit(HttpServletRequest req, Model model);

	public String list(HttpServletRequest req, Model model);

	public String remove(HttpServletRequest req, Model model);
}
