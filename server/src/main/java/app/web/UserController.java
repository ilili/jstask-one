package app.web;

import app.dto.UserFullDto;
import app.dto.UserSafeDto;
import app.services.UserServiceImpl;
import app.validator.UserFormValidator;
import app.validator.UserbBasicFormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;

//import dto.UserDTO;
//import exceptions.DuplicateUserException;
//import exceptions.UserNotFoundException;
//import security.UserDetailsServiceDAO;
//import app.services.interfaces.UserService;


@Controller("UserController")
public class UserController {

	private final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	UserFormValidator userFormValidator;

	@Autowired
	UserbBasicFormValidator userFormValidatorSafe;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(userFormValidator);
		binder.setValidator(userFormValidatorSafe);
	}

	private UserServiceImpl userService;

	@Autowired
	public void setUserService(UserServiceImpl userService) {
		this.userService = userService;
	}

	// list page
	@RequestMapping(value = {"/users/list", "/users"}, method = RequestMethod.GET)
	public String listUsers(HttpServletRequest req, Model model) {
		logger.debug("showAllUsers()");

		model.addAttribute("users", userService.findAllUsersBasic());

		return "users/list";
	}

	// show user
	@Transactional
	@RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public String showUser(@PathVariable("id") int id, Model model) {
		logger.debug("showUser() id: {}", id);

		Optional<UserSafeDto> userSafeDto = userService.findUserSafeById(id);

		if (userSafeDto.isEmpty()) {
			model.addAttribute("css", "danger");
			model.addAttribute("msg", "User not found");
		} else model.addAttribute("user", userSafeDto.get());

		return "users/show";
	}


	// show add user form
	@RequestMapping(value = "/users/add", method = RequestMethod.GET)
	public String create(HttpServletRequest req, Model model) {

		logger.debug("showAddUserForm()");

		UserFullDto user = new UserFullDto();

		Map<String, String> roles = new LinkedHashMap<String, String>();
		roles.put("CLIENT", "CLIENT");
		roles.put("ADMIN", "ADMIN");
		model.addAttribute("listOfRoles", roles);

		model.addAttribute("userForm", user);

		return "users/userform";
	}

	// save or update user
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public String saveOrUpdateUser(@ModelAttribute("userForm") @Validated UserFullDto user,
	                               BindingResult result, Model model, final RedirectAttributes redirectAttributes) {

		logger.debug("saveOrUpdateUser() : {}", user);

		if (result.hasErrors()) {
			return "users/userform";
		} else {

			userService.saveOrUpdate(user);

			redirectAttributes.addFlashAttribute("css", "success");
			if (user.isNew()) {
				redirectAttributes.addFlashAttribute("msg", "User added successfully!");
			} else {
				redirectAttributes.addFlashAttribute("msg", "User updated successfully!");
			}

			return "redirect:/users/" + user.getId();
		}
	}

//	@RequestMapping(value = {"/users/list", "/users"}, method = RequestMethod.GET)
//	public String listUsers(HttpServletRequest req, Model model) {
//		logger.debug("showAllUsers()");
//
//		model.addAttribute("users", userService.findAllUsersBasic());
//
//		return "users/list";
//	}
//
//	// save or update user
//	@RequestMapping(value = "/users/userform", method = RequestMethod.POST)
//	public String saveOrUpdateUser(@ModelAttribute("userForm") @Validated UserFullDto user,
//	                               BindingResult result, Model model, final RedirectAttributes redirectAttributes) {
//
//		logger.debug("saveOrUpdateUser() : {}", user);
//
//		if (result.hasErrors()) {
//			return "users/userform";
//		} else {
//
//			userService.saveOrUpdate(user);
//
//			redirectAttributes.addFlashAttribute("css", "success");
//			if (user.isNew()) {
//				redirectAttributes.addFlashAttribute("msg", "User added successfully!");
//			} else {
//				redirectAttributes.addFlashAttribute("msg", "User updated successfully!");
//			}
//
//			return "redirect:/users/" + user.getId();
//		}
//	}

	// show update form
	@RequestMapping(value = "/users/{id}/update", method = RequestMethod.GET)
	public String showUpdateUserForm(@PathVariable("id") int id, Model model) {

		logger.debug("showUpdateUserForm() : {}", id);

		Optional<UserFullDto> user = userService.findUserFullById(id);
		if (user.isPresent()) model.addAttribute("userForm", user);

		return "users/userform";
	}

	// TODO: 01.11.2019 check that admin can't remove himself
	// delete user
	@RequestMapping(value = "/users/{id}/delete", method = RequestMethod.POST)
	public String deleteUser(@PathVariable("id") int id, final RedirectAttributes redirectAttributes) {

		logger.debug("deleteUser() : {}", id);

		userService.delete(id);

		redirectAttributes.addFlashAttribute("css", "success");
		redirectAttributes.addFlashAttribute("msg", "User is deleted!");

		return "redirect:/users";
	}
}