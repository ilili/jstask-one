package app.web;

import app.services.UserServiceImpl;
import app.validator.UserFormValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DebugPageController {

	private final Logger logger = LoggerFactory.getLogger(DebugPageController.class);

	@Autowired
	UserFormValidator userFormValidator;

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.setValidator(userFormValidator);
	}

	private UserServiceImpl userService;

	@Autowired
	public void setUserService(UserServiceImpl userService) {
		this.userService = userService;
	}


	@RequestMapping(value = "/debug", method = RequestMethod.GET)
	public String debugPage(HttpServletRequest req, Model model) {
		logger.debug("debugPage()");

		return "other/debug";
	}

	@RequestMapping(value = "/amount", method = RequestMethod.GET)
	public String amount(Model model) {
		logger.debug("amount()");
		logger.debug(userService.toString());
		Long amount = userService.getUsersAmount();
//		Long amount = 100500L;
		logger.debug("amount = " + amount);

		model.addAttribute("amount", amount);
		return "other/debug";
	}
}
