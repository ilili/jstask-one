package app.dao;//package app.model.dao;

import app.persistence.Tariff;
import app.util.HibernateHelper;
import lombok.NonNull;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

@Repository
public class TariffDao extends AbstractDao {

	static Session session = HibernateHelper.getSession();

	@Override
	public Optional<Tariff> get(@NonNull Number id) {
		Tariff baseObject;
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			baseObject = session.get(Tariff.class, id);

			session.getTransaction().commit();
		}
		return Optional.of(baseObject);
	}

	public Integer getContractsAmount(@NonNull Number id) {
		try (Session session = HibernateHelper.getSession()) {
//			Integer rowCount = 0L;
////			if (BaseObject.class.isAssignableFrom(clazz)) {
//
//			session.beginTransaction();
//
////				Criteria criteria = session.createCriteria(Tariff.class);
////				criteria.setProjection(Projections.rowCount());
////
////				List objects = criteria.list();
////
////				if (objects != null) {
////					rowCount = (Long) objects.get(0);
////				}
//
//			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
//			CriteriaQuery<app.persistence.User> criteriaQuery = criteriaBuilder.createQuery(Tariff.class);
//			Root<app.persistence.User> root = criteriaQuery.from(app.persistence.User.class);
//			criteriaQuery.select(root).where();
//			criteriaQuery.where(criteriaBuilder.equal(root.get("id"), String.valueOf(id)));
//
//			Query<app.persistence.User> query = session.createQuery(criteriaQuery);
//			app.persistence.User objects = query.getSingleResult();
////
//			session.getTransaction().commit();
////			}

			return 0;
		}
	}


	@Transactional
	public Optional<Tariff> getById(@NonNull Number id) {
		Tariff user;

		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();
			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaQuery<Tariff> criteriaQuery = criteriaBuilder.createQuery(Tariff.class);
			Root<Tariff> root = criteriaQuery.from(Tariff.class);
			criteriaQuery.select(root).where();
			criteriaQuery.where(criteriaBuilder.equal(root.get("id"), String.valueOf(id)));

			Query<Tariff> query = session.createQuery(criteriaQuery);
			Tariff tariff = query.getSingleResult();
//
//			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
//			CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
//			Root<T> root = criteriaQuery.from(clazz);
//			criteriaQuery.select(root);
//
//			Query<T> query = session.createQuery(criteriaQuery);
//			objects = query.getResultList();

			session.getTransaction().commit();
			return Optional.of(tariff);
		}
		catch (Exception e) {
			//
		}

		return null;
	}

	public void delete(Number id) {
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();
			Optional<Tariff> optUser = getById(id);
			if (optUser.isPresent() && optUser.get().getId() != null) {
				session.delete(optUser.get());
			}
			session.getTransaction().commit();
		}
	}


//	public Optional<Tariff> get(long id) {
//		return Optional.empty();
//	}
//
//	public List<Tariff> getUserTariffs(User user) {
//		return new ArrayList<Tariff>();
//	}
//
//
//	@Override public List<Tariff> getAll() {
//		return null;
//	}
//
//	@Override public void save(Tariff tariff) {
//
//	}
//
//	@Override public void update(Tariff tariff, Map<String, Object> params) {
//
//	}
//
//	@Override public void delete(Tariff tariff) {
//
//	}
}
