package app.dao;

import app.dto.EntityIntIdDto;

import java.util.List;

public interface UserDao {

	EntityIntIdDto findById(Integer id);

	List<EntityIntIdDto> findAll();

	void save(EntityIntIdDto user);

	void update(EntityIntIdDto user);

	void delete(Integer id);

}