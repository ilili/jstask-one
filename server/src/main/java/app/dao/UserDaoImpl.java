package app.dao;

import app.util.HibernateHelper;
import lombok.NonNull;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Optional;

@Repository
public class UserDaoImpl<User> extends AbstractDao {

	@Override
	public Optional<app.persistence.User> get(@NonNull Number id) {
		app.persistence.User baseObject;
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			baseObject = session.get(app.persistence.User.class, id);

			session.getTransaction().commit();
		}
		return Optional.of(baseObject);
	}

	@Transactional
	public Optional<app.persistence.User> getById(@NonNull Number id) {
		app.persistence.User user;

		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();
			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaQuery<app.persistence.User> criteriaQuery = criteriaBuilder.createQuery(app.persistence.User.class);
			Root<app.persistence.User> root = criteriaQuery.from(app.persistence.User.class);
			criteriaQuery.select(root).where();
			criteriaQuery.where(criteriaBuilder.equal(root.get("id"), String.valueOf(id)));

			Query<app.persistence.User> query = session.createQuery(criteriaQuery);
			app.persistence.User objects = query.getSingleResult();
//
//			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
//			CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
//			Root<T> root = criteriaQuery.from(clazz);
//			criteriaQuery.select(root);
//
//			Query<T> query = session.createQuery(criteriaQuery);
//			objects = query.getResultList();

			session.getTransaction().commit();
			return Optional.of(objects);
		}
		catch (Exception e) {
			//
		}

		return null;
	}

	public void delete(Number id) {
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();
			Optional<app.persistence.User> optUser = getById(id);
			if (optUser.isPresent() && optUser.get().getId() != null) {
				session.delete(optUser.get());
			}
			session.getTransaction().commit();
		}
	}
}