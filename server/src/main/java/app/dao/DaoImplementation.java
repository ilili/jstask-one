package app.dao;

import org.springframework.stereotype.Service;

@Service
public abstract class DaoImplementation<T> extends AbstractDao<T> {}