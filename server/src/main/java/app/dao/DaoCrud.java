package app.dao;

import java.util.List;
import java.util.Optional;

public interface DaoCrud<T> {

	Optional<T> get(Number id);

	List<T> getAll(Class<T> clazz);

	void save(T t);

//	void update(T t, Map<String, Object> params);
	void update(T t);

	void delete(Number n);
}