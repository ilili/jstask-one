package app.dao;


import app.persistence.BaseObject;
import app.util.FieldsHelper;
import app.util.HibernateHelper;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.QueryParameterException;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

public abstract class AbstractDao<T> implements DaoCrud<T> {

	private Class<T> returnedClass() {
		ParameterizedType parameterizedType = (ParameterizedType) getClass()
			.getGenericSuperclass();

		@SuppressWarnings("unchecked")
		Class<T> result = (Class<T>) parameterizedType.getActualTypeArguments()[0];

		return result;
	}

	// TODO: 27.10.2019 exceptions

	// TODO: 27.10.2019 pagination selection

	@Transactional
	public List<T> getAll(Class<T> clazz) {
		List<T> objects;

		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
			CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
			Root<T> root = criteriaQuery.from(clazz);
			criteriaQuery.select(root);

			Query<T> query = session.createQuery(criteriaQuery);
			objects = query.getResultList();

			session.getTransaction().commit();
		}
		return objects;
	}

	public Long getTotalAmount(Class clazz) {
		try (Session session = HibernateHelper.getSession()) {
			Long rowCount = 0L;
			if (BaseObject.class.isAssignableFrom(clazz)) {

				session.beginTransaction();

				Criteria criteria = session.createCriteria(clazz);
				criteria.setProjection(Projections.rowCount());

				List objects = criteria.list();

				if (objects != null) {
					rowCount = (Long) objects.get(0);
				}

				session.getTransaction().commit();
			}

			return rowCount;
		}
	}

	protected String getTableNameByClass(@NonNull Class<T> clazz) {
//		if (clazz.isAssignableFrom(BaseObject.class)) {}
		String result = null;
		if ("app.persistence".equals(clazz.getPackageName())) {
			switch (clazz.getSimpleName()) {
				case "User":
					result = "Users";
					break;
				case "Contract":
					result = "Contracts";
					break;
				case "Tariff":
					result = "Tariffs";
					break;
				case "TariffOption":
					result = "Tariff_options";
					break;
				case "TariffOptionsRestrictions":
					result = "Tariff_options_restrictions";
					break;
			}
		}
		if (StringUtils.isEmpty(result)) {
			throw new QueryParameterException("Incorrect class given (" + clazz.getCanonicalName() +
				                                  "), can't translate it to the table name.");
		}
		return result;
	}


	private Class getClassByName(@NonNull String clazzName) throws ClassNotFoundException {
		return Class.forName("app.persistence." + clazzName);
	}

	public void save(T baseObject) {
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			session.save(baseObject);

			session.getTransaction().commit();
		}
	}

	@Override public void update(T baseObject) {
		try (Session session = HibernateHelper.getSession()) {
			session.beginTransaction();

			session.update(baseObject);

			session.getTransaction().commit();
		}
	}

//	@Override public void update(BaseObject baseObject, Map<String, Object> params) {
//		try (Session session = HibernateHelper.getSession()) {
//			session.beginTransaction();
//
//			if (params != null) {
//				params.entrySet()
//				      .forEach(entry -> mapSingleValue(baseObject, entry));
//			}
//			session.update(baseObject);
//
//			session.getTransaction().commit();
//		}
//	}


//	@Override public void delete(Number id) {
//		try (Session session = HibernateHelper.getSession()) {
//			session.beginTransaction();
//
//			session.delete(baseObject);
//
//			session.getTransaction().commit();
//		}
//	}


	private void mapSingleValue(Object object, Map.Entry<String, Object> mapEntry) {
		FieldsHelper.mapSingleValue(object, mapEntry);
	}
}