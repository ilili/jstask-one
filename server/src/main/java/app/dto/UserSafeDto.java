package app.dto;

import app.persistence.Contract;
import app.persistence.UserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.sql.Timestamp;
import java.util.Collection;

@Data @EqualsAndHashCode(callSuper=true)
public class UserSafeDto extends EntityIntIdDto {

	private String login;

	private String email;

	private UserRole userRole;

	private String firstName;

	private String lastName;

	private String address;

	private String passport;

	private Timestamp birthDate;

	private Collection<Contract> contracts;
}
