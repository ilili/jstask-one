package app.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data @EqualsAndHashCode(callSuper=true)
public abstract class EntityLongIdDto extends BaseObjectDto {

	public Long id;

	public boolean isNew() { return (this.id == null); }
}
