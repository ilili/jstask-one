package app.dto;

import lombok.Data;

import java.sql.Timestamp;

@Data
public abstract class BaseObjectDto {

	public boolean isActive;

	public Integer createdBy;

	protected Timestamp created;

	protected Integer changedBy;

	protected Timestamp lastChange;

}
