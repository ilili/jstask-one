package app.dto;

import app.persistence.Contract;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Collection;

@Data @EqualsAndHashCode(callSuper=true)
public class TariffDto extends EntityIntIdDto {

	private String name;

	private Double price;

	private Collection<Contract> contracts;

	private String availableOptions;
}
