package app.dto;

import app.persistence.UserRole;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data @EqualsAndHashCode(callSuper=true)
public class UserBasicDto extends EntityIntIdDto {

	String login;

	private String email;

	private UserRole userRole;

	private String firstName;

	private String lastName;
}