package app.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data @EqualsAndHashCode(callSuper=true)
public abstract class EntityIntIdDto extends BaseObjectDto {

	protected Integer id;

	public boolean isNew() { return (this.id == null); }
}
