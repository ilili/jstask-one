USE JSchool;

INSERT INTO `Users`
(`LOGIN`, `EMAIL`, `PASSWORD_HASH`, `USER_ROLE`, `IS_ACTIVE`, `FIRST_NAME`, `LAST_NAME`, `ADDRESS`, `PASSPORT`,
 `BIRTH_DATE`, `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('m.mustermann', 'max@mustermann.com', '69F7D8F45C4739F1B9E06C7DA929838C', 'ADMIN', '1', 'Maximillian',
        'Mustermann', 'Deutschland; Berlin; Ampelstraße 12', '123456 0000', '1970-12-25 00:00:00.000000', '0',
        '2015-01-01 00:00:00.000000', 0, '2015-02-02 01:01:01.000000');

INSERT INTO `Users`
(`LOGIN`, `EMAIL`, `PASSWORD_HASH`, `USER_ROLE`, `IS_ACTIVE`, `FIRST_NAME`, `LAST_NAME`, `ADDRESS`, `PASSPORT`,
 `BIRTH_DATE`, `CREATED_BY`, `CREATED`)
VALUES ('e.mustermann', 'erika@mustermann.com', '7BD652F9156D8DCF6A4F66809D258549', 'ADMIN', '1', 'Erika', 'Mustermann',
        'Deutschland; Berlin; Ampelstraße 12', '678901 0000', '1971-01-07 00:00:00.000000', '0',
        '2016-02-02 08:04:30.000000');

INSERT INTO `Users`
(`LOGIN`, `EMAIL`, `PASSWORD_HASH`, `USER_ROLE`, `IS_ACTIVE`, `FIRST_NAME`, `LAST_NAME`, `ADDRESS`, `PASSPORT`,
 `BIRTH_DATE`, `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('hanna', 'hanna@nnamretsum.com', '18851AC8F7DBDB1EE6336F251D88CB6E', 'CLIENT', '1', 'Haná', 'Jožinova',
        'Česká republika; Zlín; Nad Ovčírnou II 1290', '010101 4242', '1999-06-29 00:00:00.000000', '1',
        '2017-04-04 00:00:00.000000', 1, '2017-04-04 00:00:0.000000');

INSERT INTO `Users`
(`LOGIN`, `EMAIL`, `PASSWORD_HASH`, `USER_ROLE`, `IS_ACTIVE`, `FIRST_NAME`, `LAST_NAME`, `ADDRESS`, `PASSPORT`,
 `BIRTH_DATE`, `CREATED_BY`, `CREATED`)
VALUES ('mustafa-s', 'mustafa@nnamretsum.com', 'D419FEF9903D80B639CD0CE02172ECD0', 'CLIENT', '1', 'Mustafa', 'Selim',
        'Türkiye; İzmir; Yiğitler, 35370 Buca Osb', '223322 4455', '1985-10-02 00:00:00.000000', '1',
        '2018-01-04 09:18:33.000000');

INSERT INTO `Users`
(`LOGIN`, `EMAIL`, `PASSWORD_HASH`, `USER_ROLE`, `IS_ACTIVE`, `FIRST_NAME`, `LAST_NAME`, `ADDRESS`, `PASSPORT`,
 `BIRTH_DATE`, `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('ieva', 'ieva@nnamretsum.com', '5B936D47004BEFBD4298D3A466CBFD57', 'CLIENT', '0', 'Ieva', 'Järvinen',
        'Suomi; 99870 Inari; Kittilänratsutie 11', '852369 4569', '2000-05-17 00:00:00.000000', '0',
        '2019-09-25 13:04:02.000000', 1, '2019-10-11 19:58:06.000000');

INSERT INTO `Tariffs` (`NAME`, `PRICE`, `IS_ACTIVE`, `AVAILABLE_OPTIONS`, `CREATED_BY`, `CREATED`, `CHANGED_BY`,
                       `LAST_CHANGE`)
VALUES ('Unlimited 24', '49.99', '1',
        '{"matchers":["Double-speed-unlimited-internet-1","Triple-speed-unlimited-internet-2","Almost-no-internet-3","3G-5","6G-7","Free-traffic-to-FIDONET-6","No-internet-at-all-4"]}',
        '0', '2015-04-01 10:46:18.000000', '0', '2015-04-02 01:00:59.000000');

INSERT INTO `Tariffs` (`NAME`, `PRICE`, `IS_ACTIVE`, `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('Basic', '3.00', '1', '1', '2016-02-02 10:46:18.000000', '0', '2018-12-02 10:55:01.000000');

INSERT INTO `Tariffs` (`NAME`, `PRICE`, `IS_ACTIVE`, `CREATED_BY`, `CREATED`)
VALUES ('Advanced', '8.00', '1', '1', '2017-03-08 18:00:06.000000');

INSERT INTO `Tariffs` (`NAME`, `PRICE`, `IS_ACTIVE`, `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('Professional', '9.99', '1', '0', '2018-12-24 19:10:40.000000', '1', '2018-12-26 08:15:09.000000');

INSERT INTO `Tariffs` (`NAME`, `PRICE`, `IS_ACTIVE`, `CREATED_BY`, `CREATED`)
VALUES ('Mysterious', '42.0', '0', '1', '2018-12-31 17:50:01.000000');

INSERT INTO `Tariffs` (`NAME`, `PRICE`, `IS_ACTIVE`, `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('Solid 12', '12.12', '1', '0', '2019-06-06 15:37:06.000000', '0', '2019-07-10 16:00:57.000000');

INSERT INTO `Contracts` (`USER_ID`, `CONTRACT_NUMBER`, `PHONE_NUMBER`, `TARIFF_ID`, `TARIFF_OPTIONS`, `IS_ACTIVE`,
                         `CREATED_BY`, `CREATED`)
VALUES ('3', 'C-01', '7894563211', '3', '{}', '1', '0', '2017-04-05 11:10:18.000000');

INSERT INTO `Contracts` (`USER_ID`, `CONTRACT_NUMBER`, `PHONE_NUMBER`, `TARIFF_ID`, `TARIFF_OPTIONS`, `IS_ACTIVE`,
                         `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('3', 'C-02', '7894563212', '1', '{"matchers":["Triple-speed-unlimited-internet-2","6G-7"]}', '0', '1',
        '2017-04-06 12:59:12.000000', '1', '2019-05-01 17:30:01.000000');

INSERT INTO `Contracts` (`USER_ID`, `CONTRACT_NUMBER`, `PHONE_NUMBER`, `TARIFF_ID`, `TARIFF_OPTIONS`, `IS_ACTIVE`,
                         `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('4', 'C-03', '7368965742', '2', '{}', '1', '0', '2018-02-01 14:43:49.000000', '0',
        '2018-12-02 10:55:01.000000');

INSERT INTO `Contracts` (`USER_ID`, `CONTRACT_NUMBER`, `PHONE_NUMBER`, `TARIFF_ID`, `TARIFF_OPTIONS`, `IS_ACTIVE`,
                         `CREATED_BY`, `CREATED`)
VALUES ('4', 'C-04', '7987654321', '4', '{}', '1', '0', '2015-04-03 11:10:18.000000');

INSERT INTO `Contracts` (`USER_ID`, `CONTRACT_NUMBER`, `PHONE_NUMBER`, `TARIFF_ID`, `TARIFF_OPTIONS`, `IS_ACTIVE`,
                         `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('5', 'C-05', '7123456789', '6', '{}', '0', '0', '2019-07-01 09:10:44.000000', 1, '2019-10-11 19:58:06.000000');

INSERT INTO `Tariff_options` (`TARIFF_ID`, `NAME`, `MATCHER`, `PRICE`, `ACTIVATION_PRICE`, `IS_ACTIVE`, `CREATED_BY`,
                              `CREATED`)
VALUES ('1', 'Double speed unlimited internet', 'Double-speed-unlimited-internet-1', '99.99', '10.99', '1', '0',
        '2015-04-01 10:48:00.000000');

INSERT INTO `Tariff_options` (`TARIFF_ID`, `NAME`, `MATCHER`, `PRICE`, `ACTIVATION_PRICE`, `IS_ACTIVE`, `CREATED_BY`,
                              `CREATED`)
VALUES ('1', 'Triple speed unlimited internet', 'Triple-speed-unlimited-internet-2', '149.99', '10.99', '1', '0',
        '2015-04-01 12:31:20.000000');

INSERT INTO `Tariff_options` (`TARIFF_ID`, `NAME`, `MATCHER`, `PRICE`, `ACTIVATION_PRICE`, `IS_ACTIVE`, `CREATED_BY`,
                              `CREATED`)
VALUES ('1', 'Almost no internet', 'Almost-no-internet-3', '5.00', '3.99', '1', '0', '2015-04-01 13:00:01.000000');

INSERT INTO `Tariff_options` (`TARIFF_ID`, `NAME`, `MATCHER`, `PRICE`, `ACTIVATION_PRICE`, `IS_ACTIVE`, `CREATED_BY`,
                              `CREATED`)
VALUES ('1', 'No internet at all', 'No-internet-at-all-4', '1.00', '3.99', '1', '0', '2015-04-01 14:11:50.000000');

INSERT INTO `Tariff_options` (`TARIFF_ID`, `NAME`, `MATCHER`, `PRICE`, `ACTIVATION_PRICE`, `IS_ACTIVE`, `CREATED_BY`,
                              `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('1', '3G', '3G-5', '2.00', '3.99', '1', '0', '2015-04-01 14:40:12.000000', '0', '2018-01-01 06:32:16.000000');

INSERT INTO `Tariff_options` (`TARIFF_ID`, `NAME`, `MATCHER`, `PRICE`, `ACTIVATION_PRICE`, `IS_ACTIVE`, `CREATED_BY`,
                              `CREATED`)
VALUES ('1', 'Free traffic to FIDONET', 'Free-traffic-to-FIDONET-6', '200.00', '50.0', '1', '0',
        '2015-04-01 16:10:19.000000');

INSERT INTO `Tariff_options` (`TARIFF_ID`, `NAME`, `MATCHER`, `PRICE`, `ACTIVATION_PRICE`, `IS_ACTIVE`, `CREATED_BY`,
                              `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('1', '5G', '6G-7', '14.98', '23.00', '0', '0', '2015-04-01 14:40:12.000000', '1', '2017-11-13 01:02:05.000000');

INSERT INTO `Tariff_options_restrictions` (`TARIFF_ID`, `OPTIONS_MATCHER`, `RELATIONSHIP`, `OPTIONS_MANAGEABLE`,
                                           `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('1', '{"matchers":["No-internet-at-all-4"]}', '1',
        '{"matchers":["Double-speed-unlimited-internet-1","Triple-speed-unlimited-internet-2","Almost-no-internet-3","3G-5","6G-7"]}',
        '0', '2016-03-22 18:22:00.000000', '1', '2019-08-01 20:55:29.000000');

INSERT INTO `Tariff_options_restrictions` (`TARIFF_ID`, `OPTIONS_MATCHER`, `RELATIONSHIP`, `OPTIONS_MANAGEABLE`,
                                           `CREATED_BY`, `CREATED`)
VALUES ('1', '{"matchers":["Almost-no-internet-3"]}', '1',
        '{"matchers":["No-internet-at-all-4","Double-speed-unlimited-internet-1","Triple-speed-unlimited-internet-2","Almost-no-internet-3","3G-5","6G-7"]}',
        '1', '2017-01-08 10:00:11.000000');

INSERT INTO `Tariff_options_restrictions` (`TARIFF_ID`, `OPTIONS_MATCHER`, `RELATIONSHIP`, `OPTIONS_MANAGEABLE`,
                                           `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('1', '{"matchers":["Double-speed-unlimited-internet-1"]}', '0', '{"matchers":["6G-7"]}', '0',
        '2017-03-03 12:44:34.000000', '0', '2018-07-01 11:39:29.000000');

INSERT INTO `Tariff_options_restrictions` (`TARIFF_ID`, `OPTIONS_MATCHER`, `RELATIONSHIP`, `OPTIONS_MANAGEABLE`,
                                           `CREATED_BY`, `CREATED`)
VALUES ('1', '{"matchers":["Triple-speed-unlimited-internet-2"]}', '0', '{"matchers":["6G-7"]}', '0',
        '2018-04-29 18:46:01.000000');

INSERT INTO `Tariff_options_restrictions` (`TARIFF_ID`, `OPTIONS_MATCHER`, `RELATIONSHIP`, `OPTIONS_MANAGEABLE`,
                                           `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('1', '{"matchers":["Free-traffic-to-FIDONET-6"]}', '0', '{"matchers":["Almost-no-internet-3"]}', '1',
        '2018-04-29 19:00:50.000000', '0', '2018-05-02 15:10:03.000000');

INSERT INTO `Tariff_options_restrictions` (`TARIFF_ID`, `OPTIONS_MATCHER`, `RELATIONSHIP`, `OPTIONS_MANAGEABLE`,
                                           `CREATED_BY`, `CREATED`, `CHANGED_BY`, `LAST_CHANGE`)
VALUES ('1', '{"matchers":["Free-traffic-to-FIDONET-6"]}', '1', '{"matchers":["3G-5"]}', '1',
        '2019-02-27 05:49:00.000000', '0', '2019-02-27 06:01:13.000000');
