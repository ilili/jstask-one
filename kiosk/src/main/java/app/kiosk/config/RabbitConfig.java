package app.kiosk.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:rabbit.properties")
@Configuration @Getter
public class RabbitConfig {

	@Value("${rabbitmq.incoming.queue.name}")
	private String incomingQueueName;

	@Value("${rabbitmq.incoming.queue.durable}")
	private boolean isIncomingQueueDurable;

	@Value("${rabbitmq.exchange.name}")
	private String exchangeName;

	@Value("${rabbitmq.exchange.durable}")
	private boolean isExchangeDurable;

	@Value("${rabbitmq.exchange.autodelete}")
	private boolean isAutodeleteable;
}
