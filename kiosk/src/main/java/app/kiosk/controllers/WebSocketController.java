package app.kiosk.controllers;

import app.kiosk.dao.TariffListDao;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

@Controller
@ServerEndpoint("/tariff-socket")
public class WebSocketController {

	private static final Logger logger = LoggerFactory.getLogger(WebSocketController.class);
	@Getter @Setter private static Session peer;

	@OnOpen
	public void open(Session peer) {
		logger.info("open() : {}", peer);
		setPeer(peer);
	}

	@OnClose
	public void close(Session peer) {
		logger.info("close() : {}", peer);
		setPeer(null);
	}

	@OnMessage
	public void onMessage(String message, Session peer) {
		logger.info("onMessage() : {}", message);
	}
//
//	public void notifyAllPeers(String message) {
//		logger.info("Notifying peers with message: {}", message);
////		for (Session peer : peers) {
////			sendMessage(message, peer);
////		}
//	}

	public void sendMessage(String message) {
		logger.info("---", message, peer);
		try {
			peer.getBasicRemote().sendText(message);
			logger.info("Send text to peer: {}", peer.getId());
		}
		catch (IOException ioe) {
			logger.error("{} # {}", ioe.getMessage(), ioe.getCause());
		}
	}

	public void sendMessage(TariffListDao message) {
		logger.info("---", message, peer);
		try {
			peer.getBasicRemote().sendObject(message);
			logger.info("Send text to peer: {}", peer.getId());
		}
		catch (IOException | EncodeException ioe) {
			logger.error("{} # {}", ioe.getMessage(), ioe.getCause());
		}
	}
}
