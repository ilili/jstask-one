package app.kiosk.controllers;

import app.kiosk.services.TariffService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * Created by jt on 1/10/17.
 */
@Controller
public class BasePageController {

	private static final Logger log = LogManager.getLogger(BasePageController.class);

	private TariffService tariffService;
	private ObjectMapper objectMapper;
//	private WebSocketController webSocketController;

	@Autowired
	public BasePageController(TariffService tariffService, ObjectMapper objectMapper) {
//	public BasePageController(TariffService tariffService, ObjectMapper objectMapper, WebSocketController webSocketController) {
		this.tariffService = tariffService;
		this.objectMapper = objectMapper;
//		this.webSocketController = webSocketController;
	}
	//
//	@GetMapping(value = "/kiosk")
//	public String redirectToList() {
//		log.info("redirectToList");
//
//		return "redirect:/jsf/index.xhtml";
//	}
//
//	@GetMapping("/error")
//	public String showError() {
//		log.info("error");
//		return "redirect:/jsf/error.xhtml";
//	}

//	public void updatePage(String jsonMessage) {
//		webSocketController.sendMessage(jsonMessage);
//		log.info("publish: " + jsonMessage);
//	}

//	public void updatePage(String textMesssage) {
////		List<Tariff> tariffs = tariffService.listAll();
//
//		webSocketController.sendMessage(textMesssage);
//		log.info("publish: " + textMesssage);
//	}
}