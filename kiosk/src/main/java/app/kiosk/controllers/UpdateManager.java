package app.kiosk.controllers;

import app.kiosk.dao.TariffListDao;
import app.kiosk.listeners.TariffListener;
import app.kiosk.services.TariffServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateManager {

	private static final Logger log = LoggerFactory.getLogger(WebSocketController.class);

	private BasePageController basePageController;
	private WebSocketController webSocketController;
	private TariffListener tariffListener;
	private TariffServiceImpl tariffService;


	@Autowired
	public UpdateManager(BasePageController basePageController,
	                     WebSocketController webSocketController,
	                     TariffListener tariffListener,
	                     TariffServiceImpl tariffService) {
		this.basePageController = basePageController;
		this.webSocketController = webSocketController;
		this.tariffListener = tariffListener;
		this.tariffService = tariffService;
	}


	public void requestUpdate() {
		log.info("Requesting full update");

		// TODO: 25.11.2019 request
	}

	public void updatePage(TariffListDao tld) {
		log.info("Updating page with full list");

		webSocketController.sendMessage(tld);
	}
}
