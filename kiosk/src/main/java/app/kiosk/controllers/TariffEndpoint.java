package app.kiosk.controllers;//package app.kiosk.controllers;
//
//import app.kiosk.listeners.TariffListener;
//import app.kiosk.utils.Message;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//
//import javax.websocket.*;
//import javax.websocket.server.PathParam;
//import javax.websocket.server.ServerEndpoint;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Set;
//import java.util.concurrent.CopyOnWriteArraySet;
//
//@ServerEndpoint(value = "/tariffs")
//public class TariffEndpoint {
//	private static final Logger log = LogManager.getLogger(TariffListener.class);
//
//	private Session session;
//	private static Set<TariffEndpoint> chatEndpoints
//		= new CopyOnWriteArraySet<>();
//	private static HashMap<String, String> users = new HashMap<>();
//
//	@OnOpen
//	public void onOpen(
//		Session session,
//		@PathParam("username") String username) throws IOException, EncodeException {
//
//		this.session = session;
//		chatEndpoints.add(this);
//		users.put(session.getId(), username);
//
//		Message message = new Message();
//		message.setFrom(username);
//		message.setContent("Connected!");
//		broadcast(message);
//	}
//
//	@OnMessage
//	public void onMessage(String message, Session session)
//		throws IOException, EncodeException {
//		Message message1 = new Message();
//		message1.setFrom(users.get(session.getId()));
//		broadcast(message1);
//	}
//
//	@OnClose
//	public void onClose(Session session) throws IOException, EncodeException {
//
//		chatEndpoints.remove(this);
//		Message message = new Message();
//		message.setFrom(users.get(session.getId()));
//		message.setContent("Disconnected!");
//		broadcast(message);
//	}
//
//	@OnError
//	public void onError(Session session, Throwable throwable) {
//		// Do error handling here
//	}
//
//	private static void broadcast(Message message)
//		throws IOException, EncodeException {
//
//		chatEndpoints.forEach(endpoint -> {
//			synchronized (endpoint) {
//				try {
//					endpoint.session.getBasicRemote().
//						sendObject(message);
//				}
//				catch (IOException | EncodeException e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//}