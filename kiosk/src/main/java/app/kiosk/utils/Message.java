package app.kiosk.utils;

import lombok.Data;

@Data
public class Message {

	private String content;
}