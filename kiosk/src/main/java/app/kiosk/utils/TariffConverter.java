package app.kiosk.utils;

import app.kiosk.dao.TariffDao;
import app.kiosk.persistence.Tariff;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class TariffConverter {

	private TariffConverter() {}

	public static Tariff getPersistenceTariff(@NotNull TariffDao tariffDao) {
		Tariff tariff = FieldsHelper.copyProperties(tariffDao, new Tariff());
		tariff.setOriginalId(tariffDao.getId());
		tariff.setRecieved(Timestamp.valueOf(LocalDateTime.now()));
		return tariff;
	}

	public static TariffDao getTariffDao(@NotNull Tariff tariff) {
		TariffDao tariffDao = FieldsHelper.copyProperties(tariff, new TariffDao());
		tariffDao.setId(tariff.getOriginalId());
		return tariffDao;
	}
}
