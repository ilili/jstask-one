package app.kiosk.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class TariffListDao implements Serializable {

	@JsonProperty("tariffs")
	private List<TariffDao> tariffs;

	@JsonProperty("package_type")
	private String packageType;

	public TariffListDao addTariffs(TariffDao... tariffArray) {
		if (tariffs == null) tariffs = new ArrayList<>();

		for (TariffDao tariff : tariffArray) {
			if (!tariffs.contains(tariff)) tariffs.add(tariff);
		}

		return this;
	}

	public TariffListDao addTariffs(List<TariffDao> tariffDaoList) {
		if (tariffs == null) tariffs = new ArrayList<>();

		tariffs.addAll(tariffDaoList);

		return this;
	}
}
/*
* public class TariffListDao<T extends TariffDaoInterface> implements Serializable {

	@JsonProperty("tariffs")
	private List<T> tariffs;

	@JsonProperty("package_type")
	private String packageType;

	public TariffListDao addTariffs(T... tariffArray) {
		if (tariffs == null) tariffs = new ArrayList<>();

		for (T tariff : tariffArray) {
			if (!tariffs.contains(tariff)) tariffs.add(tariff);
		}

		return this;
	}
}

* */