package app.kiosk.dao;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class TariffDaoRecieveFromServer implements Serializable, TariffDaoInterface {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("price")
	private Double price;

	@JsonProperty("name")
	private String name;

	@JsonProperty("available_options")
	private String availableOptions;

	@JsonProperty("description")
	private String description;

	@JsonProperty("action")
	private String action;

	@JsonProperty("active_until")
	private Timestamp activeUntil;
}