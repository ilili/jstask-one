package app.kiosk.repositories;

import app.kiosk.persistence.Tariff;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.List;

public interface TariffRepository extends CrudRepository<Tariff, Long> {

	List<Tariff> getAllByOriginalIdIn(Collection<Long> originalIds);
}
