package app.kiosk.listeners;

import app.kiosk.MainApp;
import app.kiosk.controllers.UpdateManager;
import app.kiosk.dao.TariffListDao;
import app.kiosk.repositories.TariffRepository;
import app.kiosk.services.TariffServiceImpl;
import app.kiosk.utils.MessageFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Queue listener class, its receiveMessage() method invoked with the message as the parameter.
 */
@Component
public class TariffListener extends Jackson2JsonMessageConverter {

	private static final Logger logger = LogManager.getLogger(TariffListener.class);

	private TariffRepository tariffRepository;
	private ObjectMapper objectMapperLocal;
	private TariffServiceImpl tariffService;
	private UpdateManager updateManager;

	@Autowired
	public void setBasePageController(UpdateManager updateManager,
	                                  TariffServiceImpl tariffService,
	                                  ObjectMapper objectMapperLocal) {
		this.updateManager = updateManager;
		this.tariffService = tariffService;
		this.objectMapperLocal = objectMapperLocal;
	}

	public TariffListener(TariffRepository tariffRepository) {
		this.tariffRepository = tariffRepository;
	}

	/**
	 * This method is invoked whenever any new message is put in the queue. See {@link MainApp} for more details
	 *
	 * @param bytes bytes array
	 */
	public void receiveMessage(byte[] bytes) {
		String textMessage = MessageFormat.bytesToString(bytes);

		try {
			TariffListDao tld = objectMapperLocal.readValue(bytes, TariffListDao.class);
			logger.info(() -> "Received:\n" + textMessage);

			tariffService.doSomething(tld); // Updating DB
			updateManager.updatePage(tld); // Updating page
		}
		catch (IOException e) {
			logger.error(() -> "Wrong format message recieved:\n" + textMessage);
		}
	}
}
