// var container = document.getElementById('container').innerHTML;
var container = $("#container");

function isPackage(json) {
    return (typeof json.package_type !== "undefined");
}

function fillBlock(tariff) {
    var block = "<div id=\"tariff-" + tariff.id + "\" class ='card bg-light ' style=\"width: 22rem;\">";
    block += "<img class='card-img-top' src='resources\\img\\smile.png' alt='Tariff logo'>" +
        "<div class='card-body border-info'>";
    block += "<div class='tariff-name card-title'>Tariff «" + tariff.name + "»</div>" +
        "<div class='tariff-price card-text'card-subtitle mb-2 text-muted>Only " + tariff.price + " euro!</div>" +
        "<div class='tariff-description card-text text-success'>" + tariff.description + "</div>";
    // Object.entries(tariff).forEach(([key, value]) => {
    //
    //     console.log(`${key} ${value}`);
    //
    // });
    block += "</div></div>";
    console.log("block " + block);
    return block;
}

function clean_container() {
    container.empty();
}

function add_tariff(obj) {
    console.log("add " + obj);

    container.append(fillBlock(obj));
}

function remove_tariff_nocheck(tariff) {
    var id = tariff.id;
    console.log("remove " + tariff.id);

    var object_id = "tariff-" + tariff.id;
    var to_remove = ($("#" + object_id));
    if (typeof to_remove !== "undefined") {
        to_remove.remove;
        return true;
    }
    return false;
}

function remove_tariff(tariff) {
    if (tariff.action === "remove") {
        remove_tariff_nocheck(tariff);
    } else {
        console.log("can't remove element " + tariff.id);
    }
}

function update_tariff(tariff) {
    if (tariff.action === "update") {
        remove_tariff_nocheck(tariff);
        add_tariff(tariff);
    }
}

function get_tariffs_list(json) {
    if (typeof json.tariffs !== "undefined") return json.tariffs;
    else return get_empty_json();
}

function do_something(string) {
    console.log("package: " + string);
    var json = getJson(string);
    if (isPackage(json)) {
        if (json.package_type === "full") {
            console.log("full package");
            let tariffs = get_tariffs_list(json);

            tariffs.forEach(tariff => {
                add_tariff(tariff);
            });

        }
        if (json.package_type === "partial") {
            console.log("partial");

            tariffs.forEach(tariff => {
                switch (tariff.action) {
                    case ("remove") :
                        remove_tariff(tariff);
                        break;
                    case ("add") :
                        add_tariff(tariff);
                        break;
                    case ("update") :
                        remove_tariff(tariff);
                        break;
                }
            });

        }
    }
}