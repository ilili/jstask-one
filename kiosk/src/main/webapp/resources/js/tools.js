function isEmpty(string) {
  return (typeof string == "undefined" || string === null || string === "");
}

function get_empty_json() {
  return JSON.parse("{}");
}

function getJson(string) {
  if (!isEmpty(string)) return JSON.parse(string);
  else return get_empty_json();
}