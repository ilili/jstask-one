var ws_tariffs = null;
const ws_connection_interval = 10000; // ms
const ws_refresh_interval = 300000; // ms // 43200000: half of the day
const request_tariffs = "{ \"request\" : \"refresh-tariffs\" }";

var test_str = "{\n" +
    "  \"tariffs\" : [ {\n" +
    "    \"id\" : 26,\n" +
    "    \"price\" : 6.667,\n" +
    "    \"name\" : \"T26\",\n" +
    "    \"available_options\" : \"{options: none;}\",\n" +
    "    \"description\" : \"description upd full\",\n" +
    "    \"action\" : \"update\",\n" +
    "    \"active_until\" : \"2019-11-18T21:19:01.369+0000\"\n" +
    "  }, {\n" +
    "    \"id\" : 22,\n" +
    "    \"price\" : 12.00,\n" +
    "    \"name\" : \"T22\",\n" +
    "    \"available_options\" : \"{options: none;}\",\n" +
    "    \"description\" : \"description add full\",\n" +
    "    \"action\" : \"add\", \n" +
    "    \"active_until\" : \"2019-11-18T21:19:01.370+0000\"\n" +
    "  } ],\n" +
    "  \"package_type\" : \"full\"\n" +
    "}";

function connect() {
    const wsProtocol = window.location.protocol === "https:" ? "wss" : "ws";
    const wsURI = wsProtocol + '://' + window.location.host + document.location.pathname + 'tariff-socket';
    console.log("Connecting to " + wsURI);
    ws_tariffs = new WebSocket(wsURI);
    if (ws_tariffs == null) error("Failed to open websocket connection!");

    ws_tariffs.onopen = function () {
        displayMessage('Websocket connection is open');
        requestFullTariffList();
    };

    ws_tariffs.onmessage = function (event) {
        displayMessage('The response was received! ' + event.data, 'success');
        do_something(event.data);
    };

    ws_tariffs.onerror = function (event) {
        error('Error! ' + event.data);
    };

    ws_tariffs.onclose = function () {
        displayMessage('The connection was closed or timed out.');
        setTimeout(connect, ws_connection_interval * 2);
    };
}

function disconnect() {
    if (ws_tariffs !== null) {
        ws_tariffs.close();
        ws_tariffs = null;
        displayMessage("WebSocket disconnected");
    }
    error("WebSocket already closed");
}

function sendMessage(ws, message) {
    if (exists(ws) && ws.readyState === WebSocket.OPEN) {
        ws.send(message);
        displayMessage("sent: " + message, "message");
    } else {
        displayMessage('WebSocket connection is not established.', 'error');
    }
}


/////////////////////////////////
function test() {

    do_something(test_str)
}

////////////////////////////////

function requestFullTariffList() {
    this.send(request_tariffs, function () {}, ws_tariffs);
}

function error(message) {
    console.log(message);
}

function displayMessage(data, style) {
    console.log("displayMessage " + data);
}

function displayStatus(status) {
    // var currentStatus = document.getElementById('currentstatus');
    // currentStatus.value = status;
    console.log(status);
}

window.onload = function () {
    console.log("window.onload event fired");
    connect();

    setTimeout(function () {
        request_tariffs_by_timer();
    }, ws_refresh_interval);
};

function request_tariffs_by_timer() {
    // var now = new Date().getTime();
    // var frame = Math.floor(now / ws_refresh_interval) % 2; // 0 or 1

    requestFullTariffList();

    const now = new Date().getTime();
    setTimeout(request_tariffs_by_timer, ws_refresh_interval - (now % ws_refresh_interval));
}

function send(message, callback, ws) {
    this.waitForConnection(function () {
            sendMessage(ws, message);
            if (typeof callback !== 'undefined') {
                callback();
            }
        }, ws_connection_interval,
        ws);
}

function waitForConnection(callback, interval, ws) {
    if (exists(ws) && ws.readyState === WebSocket.OPEN) {
        callback();
    } else {
        const that = this;
        console.log("ws state: " + ws.readyState);
        if (ws.readyState === WebSocket.CLOSED) {
            console.log("Trying to reconnect");
            connect();
        }
        setTimeout(function () {
            that.waitForConnection(callback, interval);
        }, interval);
    }
}

function exists(obj) {
    return !(typeof obj == "undefined" || obj == null);
}

// const promoTable = $("#promoTable");
// const output = document.getElementById("log");
// const contextPath = "http://" + document.location.host + "/";
// // const wsUri = "ws://" + document.location.host + "/tariffs-publish-endpoint";
// const wsUri = "ws://" + document.location.host + "/socket";
// const socket = new WebSocket(wsUri);
//
// const optModal = document.getElementById('optionsModal');
// const optTBody = $("#optionsTBody");
// const optSpan = document.getElementById('optionsSpan');
//
// socket.onerror = function (evt) {
//     onError(evt);
// };
//
// $(function () {
//     showAll(appendTable);
// });
//
// socket.onmessage = onMessage;
//
// function onMessage() {
//     console.log("Inside onMessage");
//     showAll(appendTable);
// }
//
// function appendTable(row) {
//     console.log("Inside is appendTable method");
//     promoTable.append(
//         "<tr>" +
//         "<td>" + row.name + "</td>" +
//         "<td>" + row.sms + "</td>" +
//         "<td>" + row.minutes + "</td>" +
//         "<td>" + row.internet + "</td>" +
//         "<td>" + row.price + "</td>" +
//         "<td>" +
//         "<p id='optionBtn_" + row.id + "'>" + "<u>" + "Options" + "</u>" + "</p>" +
//         "</td>" +
//         "</tr>"
//     )
//     ;
//     $("#optionBtn_" + row.id).click(function () {
//         optTBody.empty();
//         showOptions(row.additions);
//         optModal.style.display = "block";
//     })
// }
//
// function showOptions(data) {
//     $.each(data, function (index, value) {
//         optTBody.append(
//             "<tr>" +
//             "<td>" + value.name + "</td>" +
//             "<td>" + value.price + " ₽" + "</td>" +
//             "<td>" + value.additionActivationCost + " ₽" + "</td>" +
//             "</tr>"
//         )
//     })
//
// }
//
// function showAll(action) {
//     writeToScreen("showAll" + action);
//     clearPromo();
//     let curUrl = contextPath + "/tariffs-publish-endpoint";
//     $.ajax({url: curUrl})
//         .done(function (result) {
//             handleError(result, function (result) {
//                 for (let i = 0; i < result.length; i++) {
//                     action(result[i]);
//                 }
//             })
//         })
// }
//
// function clearPromo() {
//     promoTable.empty();
// }
//
// function handleError(result, action) {
//     if (result.code == 200) {
//         action(result.body);
//     } else {
//         onError(result.body);
//     }
// }
//
// function onError(evt) {
//     // writeToScreen('<span style="color: red;">ERROR:</span> ' + ((evt == null) ? "" : evt.data) +
//     //     "<br>" + "on URI: " + wsUri);
//     writeToScreen('ERROR: ' + ((evt == null) ? "" : evt.data) +
//         "<br>" + "on URI: " + wsUri);
// }
//
// socket.onopen = function () {
//     onOpen();
// };
//
// function writeToScreen(message) {
//     output.value += message + " | ";
// }
//
// function onOpen() {
//     writeToScreen("Connected to " + wsUri);
// }
//
// optSpan.onclick = function () {
//     optModal.style.display = "none";
// };
//
// window.onclick = function (event) {
//     if (event.target == optModal) {
//         optModal.style.display = "none";
//     }
// };